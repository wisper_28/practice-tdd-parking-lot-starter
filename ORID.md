O: 

Today, I first conducted a code review of yesterday's homework, and then continued to systematically practice TDD and OOP that I learned a few days ago. 

R: 

Today I feel very fulfilled and have learned a lot of knowledge about practical development. 

I: 

A deeper understanding of the development process, as well as the iterative development process of functions, a deeper understanding of the role and advantages of test-driven development. 

D: 

After class, I will finish my homework carefully and collect more relevant materials for study. In the future development, I will also use this to regulate my development behavior.