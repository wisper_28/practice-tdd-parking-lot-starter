package com.parkinglot;


public class ParkingTicket {
    private Boolean isUsed = false;

    public Boolean getUsed() {
        return isUsed;
    }

    public void setUsed(Boolean used) {
        isUsed = used;
    }
}
