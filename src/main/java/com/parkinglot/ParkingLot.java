package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    private static final int DEFAULT_CAPACITY = 10;
    private final int capacity;
    private final HashMap<ParkingTicket, Car> ticketCarHashMap = new HashMap<>();

    public ParkingLot() {
        this.capacity = DEFAULT_CAPACITY;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public ParkingTicket park(Car car) {
        if (hasPosition()) {
            ParkingTicket parkingTicket = new ParkingTicket();
            ticketCarHashMap.put(parkingTicket, car);
            return parkingTicket;
        } else {
            throw new NoAvailablePositionException();
        }
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (!ticketCarHashMap.containsKey(parkingTicket)) {
            throw new UnRecognizedParkingTicketException();
        } else if (parkingTicket.getUsed()) {
            throw new UnRecognizedParkingTicketException();
        }
        parkingTicket.setUsed(true);
        return ticketCarHashMap.remove(parkingTicket);
    }

    public Boolean hasPosition() {
        return ticketCarHashMap.size() < capacity;
    }

}
