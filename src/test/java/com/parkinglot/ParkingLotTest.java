package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingLot.park(car);
        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        //when
        Car fetchedCar = parkingLot.fetch(ticket);
        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_two_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);
        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);
        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }


    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_a_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        //when
        parkingLot.fetch(ticket);
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
        //then
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_a_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket wrongTicket = new ParkingTicket();
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        //then
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_parking_given_a_full_parking_lot_and_a_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < 10; i++) {
            parkingLot.park(new Car());
        }
        Car car = new Car();
        //when
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));

        //then
        assertEquals("No available position.", exception.getMessage());
    }
}
